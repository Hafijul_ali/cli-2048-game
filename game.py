from board import Board
from constant import LOGO, INTRO
from controls import Control
from ui import UI
from utils import DiskIO, Logger


class Game:
    key_alnum = {
        "UP": ("8", "W", "UP"),
        "DOWN": ("2", "S", "DOWN"),
        "LEFT": ("4", "A", "LEFT"),
        "RIGHT": ("6", "D", "RIGHT"),
        "END": ("0", "Q"),
        "HELP": "5"
    }

    def __init__(self) -> None:
        self.board = Board(size=4)
        self.input = Control(game=self)
        self.ui = UI(data=self.board)
        self.utils = DiskIO("data.txt")
        self.logger = Logger("logs.txt")
        self.command = Game.key_alnum
        self.board.occupy_cell()
        self.board.occupy_cell()
        self.ui.display()

    def run(self) -> None:
        while not self.over():
            key = self.input.read()
            self.input.handle(key)
            self.board.occupy_cell()
            self.ui.wipe()
            self.ui.display()
        print("Game over, your max score was :", self.board.max_score())

    def over(self) -> bool:
        if self.board.empty_cell_count() == 0:
            return True
        return False

    def show_help(self):
        print(LOGO)
        print(INTRO)
