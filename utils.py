from datetime import datetime


class Logger:
    def __init__(self, filename: str):
        self.logfile = filename if filename.endswith(
            ".txt") else filename + ".txt"

    @classmethod
    def time_stamp(cls) -> str:
        current_time = datetime.now()
        return current_time.strftime("%m %d %Y, %H:%M:%S")

    def log(self, statement: Exception) -> None:
        with open(self.logfile, "a") as log:
            log.write("[{0}] {1} \n".format(Logger.time_stamp(), statement))


class DiskIO:
    def __init__(self, filename: str) -> None:
        self.gamefile = filename

    def save(self, data) -> None:
        with open(self.gamefile, "w") as save_file:
            for rows in data:
                for column in rows:
                    save_file.write(column)
                save_file.write("\n")

    def load(self) -> None:
        data = []
        with open(self.gamefile, "r") as saved_data:
            data_list = saved_data.read().splitlines()
        for row in data_list:
            data.append(list(row))
        return data